import React, { useState } from "react";
import "../styles/ChatRoom.css";
import useChat from "./useChat";

const TYPING = "typing";
const NOT_TYPING = "notTyping";

const ChatRoom = (props) => {
  const { roomId } = props.match.params;
  const { messages, sendMessage, socketRef, typingUser } = useChat(roomId);
  const [newMessage, setNewMessage] = useState("");
  const [isTyping, setIsTyping] = useState(false);
  const [timeoutState, setTimeoutState] = useState(null);

  const timeoutFunction = () => {
    setIsTyping(false);
    socketRef.current.emit(NOT_TYPING, {
      senderId: socketRef.current.id,
    });
  };

  const handleNewMessageChange = (event) => {
    if (!isTyping) {
      setIsTyping(true);
      socketRef.current.emit(TYPING, {
        senderId: socketRef.current.id,
      });
      setTimeoutState(setTimeout(timeoutFunction, 3000));
    } else {
      clearTimeout(timeoutState);
      setTimeoutState(setTimeout(timeoutFunction, 3000));
    }
    setNewMessage(event.target.value);
  };

  const handleSendMessage = () => {
    sendMessage(newMessage);
    setNewMessage("");
  };

  return (
    <div className="chat-room-container">
      <h1 className="room-name">Room: {roomId}</h1>
      {typingUser && (
        <h2 className={`message-item received-message`}>
          User {typingUser} is typing...
        </h2>
      )}
      <div className="messages-container">
        <ol className="messages-list">
          {messages.map((message, i) => (
            <li
              key={i}
              className={`message-item ${
                message.ownedByCurrentUser ? "my-message" : "received-message"
              }`}
            >
              {message.body}
            </li>
          ))}
        </ol>
      </div>
      <textarea
        value={newMessage}
        onChange={handleNewMessageChange}
        placeholder="Write message..."
        className="new-message-input-field"
      />
      <button onClick={handleSendMessage} className="send-message-button">
        Send
      </button>
    </div>
  );
};

export default ChatRoom;
